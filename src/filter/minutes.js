export default (value) =>  {
    const minutes = new Date(value)
    return minutes.toLocaleTimeString('it-IT')
    // return minutes.toLocaleTimeString('en-US')
}