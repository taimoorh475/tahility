export default (value) => {
    const date = new Date(value)
    var curr_date = date.getDate();
    var curr_month = date.getMonth() + 1; //Months are zero based
    var curr_year = date.getFullYear();
    return curr_year + "." + curr_month + "." + curr_date;
}