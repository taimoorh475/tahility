import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Https
import axios from './backend'

//Filter
import Date from './filter/date'
import Minutes from './filter/minutes'

Vue.filter('date', Date)
Vue.filter('minutes', Minutes)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
