import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/index.vue'
import User from './views/user.vue'
import Groups from './views/groups.vue'
import Companies from './views/companies.vue'
import Media from './views/media.vue'
import Transactions from './views/transactions.vue'
import Withdraw from './views/withdraw.vue'
import Notification from './views/notification.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Index
    },
      {
          path: '/user',
          name: 'users',
          component: User
      },
      {
          path: '/groups',
          name: 'groups',
          component: Groups
      },
      {
          path: '/companies',
          name: 'company',
          component: Companies
      },
      {
          path: '/media',
          name: 'media',
          component: Media
      },
      {
          path: '/transactions',
          name: 'transaction',
          component: Transactions
      },
      {
          path: '/withdraw',
          name: 'withdraw',
          component: Withdraw
      },
      {
          path: '/notifications',
          name: 'notification',
          component: Notification
      },
  ]
})
