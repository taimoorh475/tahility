const path = require('path');
function resolveSrc(_path) {
    return path.join(__dirname, _path)
}
module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? './'
        : '/',
};